#from wsTools import wsTools
#from util_udfTools import udfTools
from itssupp.tools.util_dbTools import dbTools
#from util_jsonInput import jsonInput

# will allow user to change different aspects of the program account (acctid, scientific program,
#  user program, purpose, and/or year)
# user enters FDs or SPs that need to be changed (either one is ok, but not mixture)
# user is prompted to what aspect of the account info needs to change.
# user verify input choices
# each sp/FD (and children) accountID is  calculated and updated in the database


class Support_changeAccountInfo():
    def __init__(self):
        self.sComment = ''
        self.errorCnt = 0
        self.errorMessage= ""
        self.accountAttributes = {
            'current_acctID': 0,
            'new_acctID': 0,
            'sciProgramID': 0,
            'sciProgram': 0,
            'purposeID': 0,
            'purpose': 0,
            'userProgramID': 0,
            'userProgram': 0,
            'yearID': 0,
            'year': 0,
            'changed_sciProgramID': 0,  #change request
            'changed_sciProgram': 0,
            'changed_purposeID': 0,
            'changed_purpose': 0,
            'changed_userProgramID': 0,
            'changed_userProgram': 0,
            'changed_yearID': 0,
            'changed_year': 0,
            'new_sciProgramID': 0,  #new-  based on request
            'new_sciProgram': 0,
            'new_purposeID': 0,
            'new_purpose': 0,
            'new_userProgramID': 0,
            'new_userProgram': 0,
            'new_yearID': 0,
            'new_year': 0
        }

        self.fdAccount = {}
        self.myDB = dbTools()


#  this is the user input from the gui that will be used to change the account info

    def processInput(self, server, function, ticketnum, spOrFd, acctID, sciPrg, purpose, userPrg, year):
        self.errorCnt = 0
        print('\n------- S T A R T ----------\n')
        if server != 'Prod' and server != 'Dev' and server != 'Int':
            self.errorCnt = 1
        else:
            self.server = server
            # set the url for routetoworkflow
            if self.server == 'Prod':
                self.route2workflowURL = "http://clarity-prd01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-prd01"
            elif self.server == 'Dev':
                self.route2workflowURL = "http://clarity-dev01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-dev01"
            elif self.server == 'Int':
                self.route2workflowURL = "http://clarity-int01.jgi-psf.org:8180/route-to-workflow"
                self.servername = "clarity-int01"
            else:
                self.route2workflowURL = ""
                self.servername = ""
                self.errorCnt = 1
                exit()

            self.myDB.connect(self.server)
            # set status comment for db update
            self.sComment = 'ITSSUPP-' + ticketnum + ' auto-updated by Support_changeAccountInfo.py'
            self.spsOrFds = spOrFd  # save the sp ids (or fd ids ) from the input
            fdIdList = self.getFDsToProcess(spOrFd)
            print (fdIdList)
            print("server: ", server)
            print("function: ", function)
            print("ticketnum: ", ticketnum)
            print("spOrFd: ", spOrFd)
            print("acctID: ", acctID)
            print("sciPrg: ", sciPrg)
            print("purpose: ", purpose)
            print("userPrg: ", userPrg)
            print("year: ", year)
            print('\n')
            if (sciPrg=='NoChange'):
                sciPrg = ''
            if (purpose=='NoChange'):
                purpose = ''
            if(userPrg=='NoChange'):
                userPrg = ''
            if (year=='NoChange'):
                year = ''
            # set status comment for db update
            if function == "changeAccount":
                self.functionText = "changeAccount"
                self.doChangeAccount(fdIdList, acctID, sciPrg, purpose, userPrg, year, testingOnly=False)
            elif function == "changeAccountTest":
                self.functionText = "changeAccountTest"
                self.doChangeAccount(fdIdList,acctID, sciPrg, purpose, userPrg, year,testingOnly=True)
            else:
                self.errorCnt = 1



    # -----------------------------------------------------
    #
    # inputs:  server
    def doChangeAccount(self, fdIdList,acctID, sciPrg, purpose, userPrg, year,testingOnly):
        first = True  # force to get account id from db for now. add code to get from user
        newAcctID = 0
        # for each fd, get account info

        for fd in fdIdList:
            #print ("FD ID: ",fd)
            # get current acct id
            fdAcctID = self.getFDAcctID(fd)
            self.accountAttributes['current_acctID'] = fdAcctID
            self.getAccountAttributes(fdAcctID)

            # get new account id based on individual attribute changes, i.e. scientific prog, purpose, user, year
            self.newAcctID = self.determineNewAcctID(first,acctID, sciPrg, purpose, userPrg, year)
            self.accountAttributes['new_acctID'] = self.newAcctID

            self.fdAccount[fd] = self.accountAttributes
            myFDattributes = self.fdAccount[fd]

            print("----------------------------------------------------------------------")
            print("FD ID: ", fd)
            print("Changing acct ID from " + myFDattributes['current_acctID'] + " to " + self.newAcctID)
            print("     current account id = " + myFDattributes['current_acctID'] +
                  " [science program = " + myFDattributes['sciProgram'] +
                  ", purpose = " + myFDattributes['purpose'] +
                  ", user program = " + myFDattributes['userProgram'] +
                  ", year = " + myFDattributes['year'] + "]")

            print("     new account id = " + self.accountAttributes['new_acctID'] +
                  " [science program = " + self.accountAttributes['new_sciProgram'] +
                  ", purpose = " + self.accountAttributes['new_purpose'] +
                  ", user program = " + self.accountAttributes['new_userProgram'] +
                  ", year = " + self.accountAttributes['new_year'] +
                  "]")

            print("")
            if (self.newAcctID != "0"):
                if (testingOnly == False):  # update db with new account Id for FD, SP, sows
                    self.updateAcctIDforFD_SPs_Sows(fd, self.newAcctID)
            else:
                self.errorCnt = 1
                self.errorMessage = "*** Error!! *** No Account ID found with attributes given!"
            first = False

        print("----------------------------------------------------------------------")
        #print out query so user can verify change in DB
        fdIDListStr = ', '.join(str(e) for e in list(fdIdList))
        #print (fdIDListStr)
        query = ("select distinct "
                 "fd.final_deliv_project_id as fdid, "
                 "fd.default_account_id as fdacctid, "
                 "sp.sequencing_project_id as spid, "
                 "sp.SEQUENCING_PROJECT_NAME as name, "
                 "sp.default_account_id as sp_acctid, "
                 "sow.SOW_ITEM_ID as sowid,"
                 "sow.account_id as sowacctid,"
                 "acct.SCIENTIFIC_PROGRAM_ID as sci_progid,"
                 "sci.SCIENTIFIC_PROGRAM as sciprog,"
                 "acct.PURPOSE_ID as purid,  "
                 "pur.PURPOSE,  "
                 " acct.USER_PROGRAM_ID as user_prog,"
                 "usr.USER_PROGRAM, "
                 "acct.YEAR_ID as yearId,   "
                 "year.year "
                 "from uss.dt_final_deliv_project fd "
                 "left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id "
                 "left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id "
                 "left join uss.dt_m2m_samplesowitem m2sam on sow.sow_item_id = m2sam.sow_item_id "
                 "left join uss.dt_sample sam on m2sam.sample_id = sam.sample_id "
                 "left join uss.dt_account acct on fd.DEFAULT_ACCOUNT_ID=acct.ACCOUNT_ID "
                 "left join uss.DT_SCIENTIFIC_PROGRAM_CV sci on sci.SCIENTIFIC_PROGRAM_ID=acct.SCIENTIFIC_PROGRAM_ID "
                 "left join USS.DT_PURPOSE_CV pur on pur.PURPOSE_ID = acct.PURPOSE_ID "
                 "left join USS.DT_USER_PROGRAM_CV usr on usr.USER_PROGRAM_ID = acct.USER_PROGRAM_ID "
                 "left join USS.DT_YEAR_CV year on year.YEAR_ID = acct.YEAR_ID "
                "where  fd.FINAL_DELIV_PROJECT_ID in (" + fdIDListStr + ");")

        print("\n\n----use this query to confirm the account info updated:----")
        print(query)
        self.confirmQuery = query

        print('\n------- D O N E ----------\n')
        return 0


    # -----------------------------------------------------


    # get the input from user, can be either SPs  or FDs,
    # separated by comma.  Will return the list of FDs associated with command  lineinput
    # inputs: none

    def getFDsToProcess(self,fdORsp):
        fdCountquery = "select count(fd.final_deliv_project_id) from uss.dt_final_deliv_project fd where fd.final_deliv_project_id in (" + fdORsp + ")"
        #print (fdCountquery)
        fdCount = self.myDB.doQuery(fdCountquery)
        fdIdList = fdORsp.split(",")
        # print(fdCount)
        if (fdCount == '0'):  # the user input are not FDs, try sps
            spCountquery = "select count(sp.sequencing_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
            #print(spCountquery)
            spCount = self.myDB.doQuery(spCountquery)
            if int(spCount) > 0:  # sps were inputted
                query = "select (sp.final_deliv_project_id) from uss.dt_sequencing_project sp where sp.sequencing_project_id in (" + fdORsp + ")"
                #print (query)
                fdIdList = self.myDB.doQueryGetAllRows(query)  # get the FDs associated with SPs
                # fdId = ",".join(map(str, set(fdIdList)))
            else:
                print("***** no FDs or SPs found in DB ******")
        return fdIdList


    # -----------------------------------------------------
    # get FD account ID
    # inputs: fdID,
    # output  acctid

    def getFDAcctID(self, fdID):
        query = "select fd.default_account_id " \
                "from uss.dt_final_deliv_project fd where  fd.final_deliv_project_id in  (" + fdID + ")"
        acctID = self.myDB.doQuery(query)  # get the acct id for  FDs
        return acctID


    # -----------------------------------------------------
    # update FD, SP and sow account ID
    # inputs: fdID, new acctid
    # output none

    def updateAcctIDforFD_SPs_Sows(self, fdID, acctID):
        # update FD acct id
        statement = "update dt_final_deliv_project " \
                    "set default_account_id=  " + str(acctID) + "," \
                    "status_comments = '" + self.sComment + "' where final_deliv_project_id in (" + fdID + ")"
        print (statement)
        self.myDB.doUpate(statement)  # update acct id for  FD

        # update SP account ids
        statement = "update dt_sequencing_project " \
                    "set default_account_id=  " + str(acctID) + "," \
                    "status_comments = '" + self.sComment + "' where final_deliv_project_id in (" + fdID + ")"
        print(statement)
        self.myDB.doUpate(statement)  # update acct id for related SPs

        # update sows account ids
        # first get the list of associated sows
        query = 'select sow.SOW_ITEM_ID from uss.dt_final_deliv_project fd ' \
                'left join uss.dt_sequencing_project sp on fd.final_deliv_project_id = sp.final_deliv_project_id ' \
                'left join uss.dt_sow_item sow on sp.sequencing_project_id = sow.sequencing_project_id ' \
                'where fd.final_deliv_project_id in (' + str(fdID) + ')'
        sowList = self.myDB.doQueryGetAllRows(query)
        sowSet = set(sowList)
        # print("SOWS:")
        # print(sowSet)
        allSows = ",".join(map(str, sowSet))  # create a string of sows items separated by comma
        statement = "update dt_sow_item " \
                    "set account_id=  " + str(acctID) + "," \
                    "status_comments = '" + self.sComment + "' where sow_item_id in (" + allSows + ") "
        print(statement)
        self.myDB.doUpate(statement)  # update acct id for related Sows


    # -----------------------------------------------------
    # get  indivial attributes of account and stores in global accountAttributes dict,
    # inputs: acctID
    # output : updates accountAttributes dict

    def getAccountAttributes(self, acctID):
        # get scientific program
        # get sci prog id
        query = "select SCIENTIFIC_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        print (query)
        sciprgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['sciProgramID'] = sciprgID
        #print ("scientific program id = ", sciprgID)
        # get cv value
        query = "select SCIENTIFIC_PROGRAM from uss.DT_SCIENTIFIC_PROGRAM_CV where SCIENTIFIC_PROGRAM_ID  in (" + sciprgID + ")"
        sciprg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['sciProgram'] = sciprg
        # print("scientific program  = ", sciprg)

        # get purpose
        # get purpose id
        query = "select PURPOSE_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        purposeID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['purposeID'] = purposeID
        # print ("PURPOSE_ID = ", purposeID)
        # get cv value
        query = "select PURPOSE from uss.DT_PURPOSE_CV where PURPOSE_ID  in (" + purposeID + ")"
        purpose = self.myDB.doQuery(query)  # get info
        self.accountAttributes['purpose'] = purpose
        # print("purpose  = ", purpose)

        # get user program
        # get user_program_id
        query = "select USER_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        userPrgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['userProgramID'] = userPrgID
        # print ("USER_PROGRAM_ID = ", userPrgID)
        # get cv value
        query = "select USER_PROGRAM from uss.DT_USER_PROGRAM_CV where USER_PROGRAM_ID  in (" + userPrgID + ")"
        userPrg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['userProgram'] = userPrg
        # print("user program  = ", userPrg)

        # get YEAR
        # get YEAR_id
        query = "select YEAR_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        yearID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['yearID'] = yearID
        # print("YEAR_ID = ", yearID)
        # get cv value
        query = "select YEAR from uss.DT_YEAR_CV where YEAR_ID  in (" + yearID + ")"
        year = self.myDB.doQuery(query)  # get info
        self.accountAttributes['year'] = year
        # print("year  = ", year, type(year))

        self.accountAttributes['new_acctID'] = acctID
        '''print("current account id = " + acctID + " [science program = " + sciprgID + "(" + sciprg +
              "), purpose = " + purposeID + "(" + purpose +
              "), user program = " + userPrgID + "(" + userPrg +
              "), year = " + yearID + "(" + year + ")]")'''




    # -----------------------------------------------------
    # get  indivial attributes of account and stores in global accountAttributes dict,
    # inputs: acctID
    # output : updates accountAttributes dict

    def getNewAccountAttributes(self, acctID):
        # get scientific program
        # get sci prog id
        query = "select SCIENTIFIC_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        sciprgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_sciProgramID'] = sciprgID
        self.accountAttributes['changed_sciProgramID'] = sciprgID
        # print ("scientific program id = ", sciprgID)
        # get cv value
        query = "select SCIENTIFIC_PROGRAM from uss.DT_SCIENTIFIC_PROGRAM_CV where SCIENTIFIC_PROGRAM_ID  in (" + sciprgID + ")"
        sciprg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_sciProgram'] = sciprg
        self.accountAttributes['changed_sciProgram'] = sciprg
        # print("scientific program  = ", sciprg)

        # get purpose
        # get purpose id
        query = "select PURPOSE_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        purposeID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_purposeID'] = purposeID
        self.accountAttributes['changed_purposeID'] = purposeID
        # print ("PURPOSE_ID = ", purposeID)
        # get cv value
        query = "select PURPOSE from uss.DT_PURPOSE_CV where PURPOSE_ID  in (" + purposeID + ")"
        purpose = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_purpose'] = purpose
        self.accountAttributes['changed_purpose'] = purpose
        # print("purpose  = ", purpose)

        # get user program
        # get user_program_id
        query = "select USER_PROGRAM_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        userPrgID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_userProgramID'] = userPrgID
        self.accountAttributes['changed_userProgramID'] = userPrgID
        # print ("USER_PROGRAM_ID = ", userPrgID)
        # get cv value
        query = "select USER_PROGRAM from uss.DT_USER_PROGRAM_CV where USER_PROGRAM_ID  in (" + userPrgID + ")"
        userPrg = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_userProgram'] = userPrg
        self.accountAttributes['changed_userProgram'] = userPrg
        # print("user program  = ", userPrg)

        # get YEAR
        # get YEAR_id
        query = "select YEAR_ID from  uss.dt_account  where  ACCOUNT_ID in (" + acctID + ")"
        yearID = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_yearID'] = yearID
        self.accountAttributes['changed_yearID'] = yearID
        # print("YEAR_ID = ", yearID)
        # get cv value
        query = "select YEAR from uss.DT_YEAR_CV where YEAR_ID  in (" + yearID + ")"
        year = self.myDB.doQuery(query)  # get info
        self.accountAttributes['new_year'] = year
        self.accountAttributes['changed_year'] = year
        # print("year  = ", year, type(year))

        print("new account id = " + acctID + " [science program = " + sciprgID + "(" + sciprg +
              "), purpose = " + purposeID + "(" + purpose +
              "), user program = " + userPrgID + "(" + userPrg +
              "), year = " + yearID + "(" + year + ")]")







    # -----------------------------------------------------
    # determine New account id from the individual  attributes ,
    # inputs: user inputs attributes to be changed and a new acctID will be determined and returned
    # output : new account id

    def determineNewAcctID(self, firstTime,acctID, sciPrg, purpose, userPrg, year):
        newAcctID = "0"

        # change individual attributes of account id
        if firstTime:  # get input
            print(acctID)
            if (acctID != 'Not Known'):  # get  new account only once,  do same for all projects
                newAcctID = acctID
                print(newAcctID, "=User inputted Acct ID. It will be applied to all FDs/SPs inputted.")
                #print("Changing acct ID to " + newAcctID)
                self.getNewAccountAttributes(newAcctID)
            else:
                print ("There is no new acct id resquested from user, find new account id using the attributes")
                self.getChangedAttributeFromUser(sciPrg, purpose, userPrg, year)
                newAcctID = self.getAccountByQuery()
        else:
            newAcctID = self.getAccountByQuery()
        if not (newAcctID):
            print("No account ID with attributes given")
            newAcctID = "0"
        return newAcctID

    # -----------------------------------------------------
    # getAccountByQuery   using current attributes and changed attributes, query DB to get account id
    # inputs: db obj
    # output : new account id

    def getAccountByQuery(self):
        # scientific program
        if self.accountAttributes['changed_sciProgramID']:
            newScienticProgID = self.accountAttributes['changed_sciProgramID']
            self.accountAttributes['new_sciProgramID'] = self.accountAttributes['changed_sciProgramID']
            self.accountAttributes['new_sciProgram'] = self.accountAttributes['changed_sciProgram']
        else:  # no changes,  store in new_
            newScienticProgID = self.accountAttributes['sciProgramID']
            self.accountAttributes['new_sciProgramID'] = self.accountAttributes['sciProgramID']
            self.accountAttributes['new_sciProgram'] = self.accountAttributes['sciProgram']
        # purpose
        if self.accountAttributes['changed_purposeID']:
            newPurposeID = self.accountAttributes['changed_purposeID']
            self.accountAttributes['new_purposeID'] = self.accountAttributes['changed_purposeID']
            self.accountAttributes['new_purpose'] = self.accountAttributes['changed_purpose']
        else:  # no changes,  store in new_
            newPurposeID = self.accountAttributes['purposeID']
            self.accountAttributes['new_purposeID'] = self.accountAttributes['purposeID']
            self.accountAttributes['new_purpose'] = self.accountAttributes['purpose']

        # user program
        if self.accountAttributes['changed_userProgramID']:
            newUserPrgID = self.accountAttributes['changed_userProgramID']
            self.accountAttributes['new_userProgramID'] = self.accountAttributes['changed_userProgramID']
            self.accountAttributes['new_userProgram'] = self.accountAttributes['changed_userProgram']
        else:  # no changes,  store in new_
            newUserPrgID = self.accountAttributes['userProgramID']
            self.accountAttributes['new_userProgramID'] = self.accountAttributes['userProgramID']
            self.accountAttributes['new_userProgram'] = self.accountAttributes['userProgram']

        # year
        if self.accountAttributes['changed_yearID']:
            newYearID = self.accountAttributes['changed_yearID']
            self.accountAttributes['new_yearID'] = self.accountAttributes['changed_yearID']
            self.accountAttributes['new_year'] = self.accountAttributes['changed_year']
        else:  # no changes,  store in new_
            newYearID = self.accountAttributes['yearID']
            self.accountAttributes['new_yearID'] = self.accountAttributes['yearID']
            self.accountAttributes['new_year'] = self.accountAttributes['year']


        # get new account id
        query = "select account_id from  uss.dt_account  where  SCIENTIFIC_PROGRAM_ID= " + newScienticProgID + \
                " and  PURPOSE_ID= " + newPurposeID + " and USER_PROGRAM_ID= " + newUserPrgID + " and YEAR_ID= " + newYearID
        print(query)
        newAcctID = self.myDB.doQuery(query)  # get the acct id for  FDs
        print(newAcctID)
        print("\nNew Account ID=",newAcctID,": Scientific Program=", newScienticProgID, "Purpose=", newPurposeID, "User Program=", newUserPrgID,
             "Year=", newYearID)
        return newAcctID


    # -----------------------------------------------------
    # getChangedAttributeFromUser
    # determine New account id from the individual  attributes ,
    # inputs: user inputs attributes to be changed and a new acctID will be determined and returned
    # output : new account id

    def getChangedAttributeFromUser(self,sciPrg, purpose, userPrg, year):

        # ---scientific program
        newScienticProg = sciPrg
        self.accountAttributes['changed_sciProgram'] = newScienticProg
        query = "select SCIENTIFIC_PROGRAM_id from uss.DT_SCIENTIFIC_PROGRAM_CV where  SCIENTIFIC_PROGRAM = '" + newScienticProg + "'"
        print(query)
        newScienticProgID = self.myDB.doQuery(query)
        print (newScienticProgID)
        self.accountAttributes['changed_sciProgramID'] = newScienticProgID

        # --- purpose
        newPurpose = purpose
        self.accountAttributes['changed_purpose'] = newPurpose
        query = "select PURPOSE_ID from uss.DT_PURPOSE_CV where  PURPOSE = '" + newPurpose + "'"
        print(query)
        newPurposeID = self.myDB.doQuery(query)
        print(newPurposeID)
        self.accountAttributes['changed_purposeID'] = newPurposeID

        # --- user program
        newUserPrg = userPrg
        self.accountAttributes['changed_userProgram'] = newUserPrg
        query = "select USER_PROGRAM_ID from uss.DT_USER_PROGRAM_CV where  USER_PROGRAM = '" + newUserPrg + "'"
        print(query)
        newUserPrgID = self.myDB.doQuery(query)
        self.accountAttributes['changed_userProgramID'] = newUserPrgID

        # --- year
        newYear = year
        self.accountAttributes['changed_year'] = newYear
        query = "select YEAR_ID from uss.DT_YEAR_CV where  YEAR = '" + newYear + "'"
        print(query)
        newYearID = self.myDB.doQuery(query)
        print("got the year")
        self.accountAttributes['changed_yearID'] = str(newYearID)

        print("done with getting new attributes from database")



#-------------------------------------------------------------------------------------------
# run tests

#myTest = Support_changeAccountInfo()

#myTest.doChangeAccount()
