from django.shortcuts import render
from itssupp.controllers.Support_abandonFDorSPandChildren import Support_abandonFDorSPandChildren
from itssupp.controllers.Support_changeAccountInfo import Support_changeAccountInfo

def index(request):
    #template = loader.get_template('itssupp/index.html')
    return render(request, 'itssupp/index.html')


def gui_abandonPage(request):
  return render(request, 'itssupp/gui_abandonPage.html')

def gui_changeAccountPage(request):
  return render(request, 'itssupp/gui_changeAccountPage.html')


# this gets all the data from the input form and sends to abandonSPreport.html or abandonFDreport.html
def infoAbandon(request):
    mySupportTask = Support_abandonFDorSPandChildren()
    form = request.GET   # get the input from the form on the page
    servername = form['server']
    ticketnum = form['ticketnum']
    function = form['function']
    spOrFd = form['SpsOrFds']
    aps = form['Aps']

    mySupportTask.doAbandon(servername, ticketnum, function, spOrFd,aps)
    NumErrors = str(mySupportTask.errorCnt)
    taskServer = mySupportTask.server
    taskDBcomment = mySupportTask.sComment
    taskFunction = mySupportTask.functionText
    sampleSet = mySupportTask.sampleSet
    sowSet = mySupportTask.sowSet
    spSet = mySupportTask.spSet
    confirmQuery = mySupportTask.confirmQuery
    workflowURL = mySupportTask.route2workflowURL
    taskServerName = mySupportTask.servername

    if (function == "SPchildren" or function == "SPchildrenTest"):
        return render(request, 'itssupp/abandonSPreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'errors': mySupportTask.errorCnt,
                                                         'errormsg': mySupportTask.errorMessage,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})
    elif (function == "FDchildren" or function == "FDchildrenTest"):   # (function == "FDchildren" or function == "FDchildrenTest"):
        apSet = mySupportTask.apSet
        atSet = mySupportTask.atSet
        fdSet = mySupportTask.fdSet
        return render(request, 'itssupp/abandonFDreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'errors': mySupportTask.errorCnt,
                                                         'errormsg': mySupportTask.errorMessage,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'aps': apSet,
                                                         'ats': atSet,
                                                         'fds': fdSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})
    else: #(function == "APandSPchildren" or function == "APandSPchildrenTest")
        apSet = mySupportTask.apSet
        atSet = mySupportTask.atSet
        return render(request, 'itssupp/abandonAPandSPreport.html', {'server': taskServer,
                                                                'comment': taskDBcomment,
                                                                'errors': mySupportTask.errorCnt,
                                                                'errormsg': mySupportTask.errorMessage,
                                                                'function': taskFunction,
                                                                'samples': sampleSet,
                                                                'sows': sowSet,
                                                                'sps': spSet,
                                                                'aps': apSet,
                                                                'ats': atSet,
                                                                'query': confirmQuery,
                                                                'url': workflowURL,
                                                                'jiraTicket': ticketnum,
                                                                'serverName': taskServerName})

#-------------------------------------------------------------------------------
#Change Account Info
# this gets all the data from the input form and sends to changeAccountInfoMOre
def infoChangeAccount(request):
    mySupportTask = Support_changeAccountInfo()
    form = request.GET   # get the input from the form on the page
    servername = form['server1']
    ticketnum = form['ticketnum']
    function = form['function']
    spOrFd = form['SpsOrFds']
    acctID = form['acctID']
    sciPrg = form['sciPrg']
    purpose = form['purpose']
    userPrg = form['userPrg']
    year = form['year']

    #process the input
    mySupportTask.processInput(servername, function, ticketnum, spOrFd, acctID, sciPrg, purpose, userPrg, year)
    #set up for output

    #sends data for output page
    return render(request, 'itssupp/changeAcctInfoReport.html', {'server': mySupportTask.servername,
                                                                    'function': mySupportTask.functionText,
                                                                    'jiraTicket': ticketnum,
                                                                    'errors': mySupportTask.errorCnt,
                                                                    'errormsg': mySupportTask.errorMessage,
                                                                    'query': mySupportTask.confirmQuery,
                                                                    'acctID': mySupportTask.newAcctID,
                                                                    'sciPrg': mySupportTask.accountAttributes['new_sciProgram'],
                                                                    'purpose': mySupportTask.accountAttributes['new_purpose'] ,
                                                                    'userPrg': mySupportTask.accountAttributes['new_userProgram'] ,
                                                                    'year': mySupportTask.accountAttributes['new_year'],
                                                                    'spIds': spOrFd
                                                                   })










#--------------------- this is used for demo only----------------------
'''def info(request):
    mySupportTask = Support_abandonFDorSPandChildren()
    form = request.GET   # get the input from the form on the page
    servername = form['server']
    ticketnum = form['ticketnum']
    function = form['function']
    spOrFd = form['SpsOrFds']

    mySupportTask.doAbandon(servername, ticketnum, function, spOrFd)
    NumErrors = str(mySupportTask.errorCnt)
    taskServer = mySupportTask.server
    taskDBcomment = mySupportTask.sComment
    taskFunction = mySupportTask.functionText
    sampleSet = mySupportTask.sampleSet
    sowSet = mySupportTask.sowSet
    spSet = mySupportTask.spSet
    confirmQuery = mySupportTask.confirmQuery
    workflowURL = mySupportTask.route2workflowURL
    taskServerName = mySupportTask.servername

    if (function == "SPchildren" or function == "SPchildrenTest"):
        return render(request, 'itssupp/abandonSPreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})
    else :   # (function == "FDchildren" or function == "FDchildrenTest"):
        apSet = mySupportTask.apSet
        atSet = mySupportTask.atSet
        fdSet = mySupportTask.fdSet
        return render(request, 'itssupp/abandonFDreport.html', {'server': taskServer,
                                                         'comment':taskDBcomment,
                                                         'function':taskFunction,
                                                         'samples':sampleSet,
                                                         'sows':sowSet,
                                                         'sps':spSet,
                                                         'aps': apSet,
                                                         'ats': atSet,
                                                         'fds': fdSet,
                                                         'query':confirmQuery,
                                                         'url': workflowURL,
                                                         'jiraTicket':ticketnum,
                                                         'serverName': taskServerName})'''

























