from django.urls import path

from . import views
app_name = 'itssupp'


urlpatterns = [
    path('', views.index, name='index'),
    path('gui_abandonPage', views.gui_abandonPage, name='gui_abandonPage'),
    path('gui_changeAccountPage/', views.gui_changeAccountPage, name='gui_changeAccountPage'),
    #path('info/', views.info),  #not used , keep for example
    path('infoAbandon/', views.infoAbandon, ),
    path('infoChangeAccount/', views.infoChangeAccount),

]